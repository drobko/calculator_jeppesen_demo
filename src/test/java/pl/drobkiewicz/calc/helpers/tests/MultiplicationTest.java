package pl.drobkiewicz.calc.helpers.tests;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import pl.drobkiewicz.calc.helpers.CalculationHelper;
import pl.drobkiewicz.calc.helpers.impl.CalculationHelperImpl;

public class MultiplicationTest {
	
	@Test
	public void standardTest() {
		
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = new BigDecimal(1);
		
		numberToTest.add(new BigDecimal(1));
		numberToTest.add(new BigDecimal(1));
		
		for (BigDecimal bigDecimal : numberToTest) {
			exptectedValue = exptectedValue.multiply(bigDecimal);
		}
		
		assertEquals(exptectedValue, helper.multiplyNumbers(numberToTest));
	}
	
	@Test
	public void testSmallValues() {
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = new BigDecimal(1);
		
		for(int i=1 ; i<500; i++) {
			BigDecimal value = new BigDecimal(1/i);
			numberToTest.add(value);
			exptectedValue = exptectedValue.multiply(value);
		}
		
		assertEquals(exptectedValue, helper.multiplyNumbers(numberToTest));
	}
	
	@Test
	public void testBigValues() {
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = new BigDecimal(1);
		
		for (int i=1;i<5000;i++) {
			BigDecimal value = new BigDecimal(i*i);
			numberToTest.add(value);
			exptectedValue = exptectedValue.multiply(value);
		}
		
		assertEquals(exptectedValue, helper.multiplyNumbers(numberToTest));
	}
	
	@Test
	public void testLargeNumberOfArguments() {
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = new BigDecimal(1);
		
		for (int i=1;i<5000;i++) {
			BigDecimal value = new BigDecimal(i);
			numberToTest.add(value);
			exptectedValue = exptectedValue.multiply(value);
		}
		
		assertEquals(exptectedValue, helper.multiplyNumbers(numberToTest));
	}
	
	@Test
	public void testRandomValues() {
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = new BigDecimal(1);
		
		for (int i=1;i<5000;i++) {
			BigDecimal random = new BigDecimal(Math.random());
			numberToTest.add(random);
			exptectedValue = exptectedValue.multiply(random);
		}
		
		assertEquals(exptectedValue, helper.multiplyNumbers(numberToTest));
	}
}
