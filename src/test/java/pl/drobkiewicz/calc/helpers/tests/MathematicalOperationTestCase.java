package pl.drobkiewicz.calc.helpers.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AdditionTest.class, 
				MultiplicationTest.class,
				SubtractionTest.class,
				DivisionTest.class})
/**
 * Here are runned all attaches tests.
 * @author filip drobkiewicz
 */
public class MathematicalOperationTestCase {

}
