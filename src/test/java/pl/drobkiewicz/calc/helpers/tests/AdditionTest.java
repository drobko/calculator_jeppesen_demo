package pl.drobkiewicz.calc.helpers.tests;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import pl.drobkiewicz.calc.helpers.CalculationHelper;
import pl.drobkiewicz.calc.helpers.impl.CalculationHelperImpl;

public class AdditionTest {
	
	@Test
	public void standardTest() {
		
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = new BigDecimal(0);
		
		numberToTest.add(new BigDecimal(1));
		numberToTest.add(new BigDecimal(1));
		
		for (BigDecimal bigDecimal : numberToTest) {
			exptectedValue = exptectedValue.add(bigDecimal);
		}
		
		assertEquals(exptectedValue, helper.addNumbers(numberToTest));
	}
	
	@Test
	public void testSmallValues() {
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = new BigDecimal(0);
		
		for(int i=1 ; i<500; i++) {
			BigDecimal value = new BigDecimal(1/i);
			numberToTest.add(value);
			exptectedValue = exptectedValue.add(value);
		}
		
		assertEquals(exptectedValue, helper.addNumbers(numberToTest));
	}
	
	@Test
	public void testBigValues() {
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = new BigDecimal(0);
		
		for (int i=1;i<5000;i++) {
			BigDecimal value = new BigDecimal(i*i);
			numberToTest.add(value);
			exptectedValue = exptectedValue.add(value);
		}
		
		assertEquals(exptectedValue, helper.addNumbers(numberToTest));
	}
	
	@Test
	public void testLargeNumberOfArguments() {
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = new BigDecimal(0);
		
		for (int i=1;i<5000;i++) {
			BigDecimal value = new BigDecimal(i);
			numberToTest.add(value);
			exptectedValue = exptectedValue.add(value);
		}
		
		assertEquals(exptectedValue, helper.addNumbers(numberToTest));
	}
	
	@Test
	public void testRandomValues() {
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = new BigDecimal(0);
		
		for (int i=1;i<5000;i++) {
			BigDecimal random = new BigDecimal(Math.random());
			numberToTest.add(random);
			exptectedValue = exptectedValue.add(random);
		}
		
		assertEquals(exptectedValue, helper.addNumbers(numberToTest));
	}
}
