package pl.drobkiewicz.calc.helpers.tests;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import pl.drobkiewicz.calc.helpers.CalculationHelper;
import pl.drobkiewicz.calc.helpers.impl.CalculationHelperImpl;

public class DivisionTest {
	
	@Test
	public void standardTest() {
		
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		
		BigDecimal val1 = new BigDecimal(5);
		BigDecimal val2 = new BigDecimal(2);
		BigDecimal val3 = new BigDecimal(1);
		BigDecimal exptectedValue = val1.divide(val2);
		exptectedValue=exptectedValue.divide(val3);
		numberToTest.add(val1);
		numberToTest.add(val2);
		numberToTest.add(val3);
		
		assertEquals(exptectedValue.stripTrailingZeros(), helper.divideNumbers(numberToTest).stripTrailingZeros());
	}
	
	@Test
	public void testSmallValues() {
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = null;
		
		for(int i=5 ; i<20; i++) {
			BigDecimal value = BigDecimal.ONE.subtract(new BigDecimal(i));
			if (exptectedValue == null) {
				exptectedValue = value;
				numberToTest.add(value);
			} else {
				numberToTest.add(value);
				exptectedValue = exptectedValue.divide(value,CalculationHelperImpl.NON_QUANTIFIBLE_NUMBER_ACCURACY,CalculationHelperImpl.ROUNDING_MODE);
			}
		}
		
		assertEquals(exptectedValue.stripTrailingZeros(), helper.divideNumbers(numberToTest).stripTrailingZeros());
	}
	
	@Test
	public void testBigValues() {
		CalculationHelper helper = new CalculationHelperImpl();
		List<BigDecimal> numberToTest = new ArrayList<BigDecimal>();
		BigDecimal exptectedValue = null;
		
		for (int i=1;i<10;i++) {
			BigDecimal value = new BigDecimal(i*i);
			if (exptectedValue == null) {
				exptectedValue = value;
				numberToTest.add(value);
			} else {
				numberToTest.add(value);
				exptectedValue = exptectedValue.divide(value,CalculationHelperImpl.NON_QUANTIFIBLE_NUMBER_ACCURACY,CalculationHelperImpl.ROUNDING_MODE);
			}
		}
		
		assertEquals(exptectedValue.stripTrailingZeros(), helper.divideNumbers(numberToTest).stripTrailingZeros());
	}
	
}
