package pl.drobkiewicz.calc.rest.controllers;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pl.drobkiewicz.calc.dao.MathOperationsResultsDao;
import pl.drobkiewicz.calc.enums.InvalidParameterExceptionTypes;
import pl.drobkiewicz.calc.enums.MathematicalOperationEnum;
import pl.drobkiewicz.calc.exceptions.InvalidParametersException;
import pl.drobkiewicz.calc.helpers.CalculationHelper;

/**
 * Abstract REST controller with methods used by controllers making mathematical operations. 
 * @author filip drobkiewicz
 */
@RestController
public abstract class AbstractMathOperationRestController {
	
	@Autowired
	protected CalculationHelper calculationHelper;
	
	@Autowired
	private MathOperationsResultsDao mathOperationDao;
	
	/**
	 * Void which will be runned when we calling POST Method in your controller, to make calculation. Return result of operation.
	 * @param numbersStringArray
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@ExceptionHandler({ InvalidParametersException.class })
	@RequestMapping(value="/", method = RequestMethod.POST, consumes = "application/json")
	protected ResponseEntity calc(@RequestBody String[] numbersStringArray) {
		
		try {			
			List<BigDecimal> numbers = convertParams(numbersStringArray);
			validateParams(numbers);
			BigDecimal result = makeCalculation(numbers);
			storeResult(numbers, result);
			return ResponseEntity
		            .status(HttpStatus.OK)                 
		            .body(result);
		} catch (InvalidParametersException e) {
			return ResponseEntity
		            .status(HttpStatus.BAD_REQUEST)                 
		            .body(e.getMessage());
		}
	};
	
	/**
	 * Abstract method to call appropriate mathematical operation.
	 * @param numbers
	 * @return
	 */
	abstract BigDecimal makeCalculation(List<BigDecimal> numbers);
	
	/**
	 * Method,  to make validation of correctnes params.
	 * @param numbers
	 */
	protected void validateParams(List<BigDecimal> numbers) {
		if (numbers.size() <=1) {
			//we need at least 2 arguments to make operation
			throw new InvalidParametersException(InvalidParameterExceptionTypes.WRONG_NUMBERS_OF_PARAMS);
		};
	}
	
	/**
	 * Method which convert data sent in response body to java list of BigDecimals.
	 * I decided to use BigDecimal class to store values with maximal precision and prevent misrepresented which can may place, when we using doubles.
	 * @param params
	 * @return
	 */
	private List<BigDecimal> convertParams(String[] params) {
		try {
			List<BigDecimal> result = new ArrayList<BigDecimal>();
			Arrays.stream(params).forEach(number -> {
				result.add(new BigDecimal(number));
			});
			return result;
		} catch (NumberFormatException e) {
			throw new InvalidParametersException(InvalidParameterExceptionTypes.INVALID_PARAMETERS_TYPE);
		} catch (NullPointerException e) {
			throw new InvalidParametersException(InvalidParameterExceptionTypes.WRONG_NUMBERS_OF_PARAMS);
		}
		
	};
	
	protected abstract MathematicalOperationEnum getOperation();
	
	private void storeResult(List<BigDecimal> numbers, BigDecimal result){
		mathOperationDao.storeNewResult(numbers, result, getOperation(), LocalDateTime.now());
	}
	
}
