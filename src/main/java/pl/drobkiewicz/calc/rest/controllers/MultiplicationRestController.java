package pl.drobkiewicz.calc.rest.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.drobkiewicz.calc.enums.MathematicalOperationEnum;

/**
 * REST controller to multiplication numbers
 * @author filip drobkiewicz
 */
@RestController
@RequestMapping("/multiplication")
public class MultiplicationRestController extends AbstractMathOperationRestController {

	@Override
	BigDecimal makeCalculation(List<BigDecimal> numbers) {
		return calculationHelper.multiplyNumbers(numbers);
	}
	
	@Override
	protected MathematicalOperationEnum getOperation() {
		return MathematicalOperationEnum.MULTIPLICATION;
	}
	
}
