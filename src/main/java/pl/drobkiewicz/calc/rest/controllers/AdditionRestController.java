package pl.drobkiewicz.calc.rest.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.drobkiewicz.calc.enums.MathematicalOperationEnum;

/**
 * REST controller to addition numbers
 * @author filip drobkiewicz
 */
@RestController
@RequestMapping("/addition")
public class AdditionRestController extends AbstractMathOperationRestController {

	@Override
	BigDecimal makeCalculation(List<BigDecimal> numbers) {
		return calculationHelper.addNumbers(numbers);
	}

	@Override
	protected MathematicalOperationEnum getOperation() {
		return MathematicalOperationEnum.ADDITION;
	}
	
}
