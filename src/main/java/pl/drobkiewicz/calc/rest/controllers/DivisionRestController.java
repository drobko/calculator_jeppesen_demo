package pl.drobkiewicz.calc.rest.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.drobkiewicz.calc.enums.InvalidParameterExceptionTypes;
import pl.drobkiewicz.calc.enums.MathematicalOperationEnum;
import pl.drobkiewicz.calc.exceptions.InvalidParametersException;

/**
 * REST controller to division numbers
 * @author filip drobkiewicz
 */
@RestController
@RequestMapping("/divide")
public class DivisionRestController extends AbstractMathOperationRestController {
	
	@Override
	BigDecimal makeCalculation(List<BigDecimal> numbers) {
		return calculationHelper.divideNumbers(numbers);
	}

	/**
	 * Overrided method,  to make validation of correctnes params.
	 * @param numbers
	 */
	@Override
	protected void validateParams(List<BigDecimal> numbers) {
		super.validateParams(numbers);
		if (numbers.stream().filter(num -> num.equals(BigDecimal.ZERO)).findAny().isPresent()) {
			//cannot dividing by zero
			throw new InvalidParametersException(InvalidParameterExceptionTypes.CANNOT_DIVIDE_BY_ZERO);
		}
	}

	@Override
	protected MathematicalOperationEnum getOperation() {
		return MathematicalOperationEnum.DIVISION;
	}
	
}
