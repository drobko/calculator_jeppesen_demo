package pl.drobkiewicz.calc.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import pl.drobkiewicz.calc.dao.entity.MathOperationResult;
import pl.drobkiewicz.calc.enums.MathematicalOperationEnum;

/**
 * Data Access Object for {@link MathOperationResult}
 * @author filip drobkiewicz
 *
 */
public interface MathOperationsResultsDao {

	/**
	 * Method to store new value
	 * @param numbers
	 * @param result
	 * @param operation
	 * @param calculatedAt
	 */
	public void storeNewResult(List<BigDecimal> numbers, BigDecimal result, MathematicalOperationEnum operation, LocalDateTime calculatedAt);
	
	/**
	 * Method to fetching all already existing records from database
	 * @return
	 */
	public List<MathOperationResult> gatAll();
	
}
