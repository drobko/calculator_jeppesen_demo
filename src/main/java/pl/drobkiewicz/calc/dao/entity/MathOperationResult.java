package pl.drobkiewicz.calc.dao.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import pl.drobkiewicz.calc.enums.MathematicalOperationEnum;

public class MathOperationResult {

	private Long id;
	private List<BigDecimal> numbersUsedInCalculation;
	private BigDecimal result;
	private LocalDateTime calculatedAt;
	private MathematicalOperationEnum operation;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<BigDecimal> getNumbersUsedInCalculation() {
		return numbersUsedInCalculation;
	}
	public void setNumbersUsedInCalculation(List<BigDecimal> numbersUsedInCalculation) {
		this.numbersUsedInCalculation = numbersUsedInCalculation;
	}
	public BigDecimal getResult() {
		return result;
	}
	public void setResult(BigDecimal result) {
		this.result = result;
	}
	public LocalDateTime getCalculatedAt() {
		return calculatedAt;
	}
	public void setCalculatedAt(LocalDateTime calculatedAt) {
		this.calculatedAt = calculatedAt;
	}
	public MathematicalOperationEnum getOperation() {
		return operation;
	}
	public void setOperation(MathematicalOperationEnum operation) {
		this.operation = operation;
	}
	@Override
	public String toString() {
		return "MathOperationResult [id=" + id + ", numbersUsedInCalculation=" + numbersUsedInCalculation + ", result="
				+ result + ", calculatedAt=" + calculatedAt + ", operation=" + operation + "]";
	}
	
}
