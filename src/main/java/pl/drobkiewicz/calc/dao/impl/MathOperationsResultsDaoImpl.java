package pl.drobkiewicz.calc.dao.impl;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import pl.drobkiewicz.calc.dao.MathOperationsResultsDao;
import pl.drobkiewicz.calc.dao.entity.MathOperationResult;
import pl.drobkiewicz.calc.enums.MathematicalOperationEnum;

@Service
public class MathOperationsResultsDaoImpl implements MathOperationsResultsDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final Logger log = LoggerFactory.getLogger(MathOperationsResultsDaoImpl.class);

	@Override
	public void storeNewResult(List<BigDecimal> numbers, BigDecimal result, MathematicalOperationEnum operation, LocalDateTime calculatedAt) {
		
		long startedAt = System.currentTimeMillis();
		
		List<String> numbersStringList = new ArrayList<String>();
		numbers.forEach(number -> {
			numbersStringList.add(number.toPlainString());
		});
		
		jdbcTemplate.update("INSERT INTO math_operation_result(id, params, result, calculated_at,math_operation) "
				+ "VALUES(DEFAULT,?,?,DEFAULT,?)", numbersStringList.toArray(), result.toPlainString(), operation.name());
		
		log.debug("Storing new values in " + (System.currentTimeMillis() - startedAt) + " ms.");
	}

	@Override
	public List<MathOperationResult> gatAll() {
		long startedAt = System.currentTimeMillis();
		List<MathOperationResult> result = jdbcTemplate.query("SELECT * FROM math_operation_result", new MathOperationMapper());
		log.debug("gatAllValues() in " + (System.currentTimeMillis() - startedAt) + " ms.");
		return result;
	}
	
	/**
	 * Mapper for {@link MathOperationResult} from database object.
	 * @author filip drobkiewicz
	 */
	private static final class MathOperationMapper implements RowMapper<MathOperationResult> {
		public MathOperationResult mapRow(ResultSet rs, int rowNum) throws SQLException {
			MathOperationResult mathOperationResult = new MathOperationResult();
			mathOperationResult.setId(rs.getLong("id"));
			
			Array array = rs.getArray("params");
			Object[] numbersArray = (Object[])array.getArray();
			List<BigDecimal> numbers = new ArrayList<BigDecimal>();
			
			Arrays.stream(numbersArray).forEach(obj -> {
				numbers.add(new BigDecimal(obj.toString().trim()));
			});
			
			mathOperationResult.setNumbersUsedInCalculation(numbers);
			mathOperationResult.setResult(new BigDecimal(rs.getString("result")));
			mathOperationResult.setCalculatedAt(rs.getTimestamp("calculated_at").toLocalDateTime());
			mathOperationResult.setOperation(MathematicalOperationEnum.getByName(rs.getString("math_operation")).get());

			return mathOperationResult;
		}
	}

}
