package pl.drobkiewicz.calc.exceptions;

import pl.drobkiewicz.calc.enums.InvalidParameterExceptionTypes;

/**
 * Exception, which are throwed, when data send to API are incorrect or incomplete.
 * @author filip drobkiewicz
 *
 */
public class InvalidParametersException extends NumberFormatException {

	private static final long serialVersionUID = 6976643050009683703L;

	public InvalidParametersException(InvalidParameterExceptionTypes type) {
		super(type.getMessage());
	}
	
}
