package pl.drobkiewicz.calc.schedullers;
//package pl.drobkiewicz.calc.scheduller;
//
//import java.util.List;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import pl.drobkiewicz.calc.dao.MathOperationsResultsDao;
//import pl.drobkiewicz.calc.dao.entity.MathOperationResult;
//
//Only to testing correctness of storing data in database.
//
//@Component
//public class Scheduller {
//
//	private static final Logger log = LoggerFactory.getLogger(Scheduller.class);
//	
//	@Autowired
//	MathOperationsResultsDao dao;
//	
//	@Scheduled(fixedDelay = 1000)
//	public void logCurrentExistingUsers() {
//		List<MathOperationResult> list = dao.gatAll();
//		log.info(list.toString());
//	}
//	
//}
