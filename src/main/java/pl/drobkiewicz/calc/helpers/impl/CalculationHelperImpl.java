package pl.drobkiewicz.calc.helpers.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import pl.drobkiewicz.calc.helpers.CalculationHelper;

@Service
public class CalculationHelperImpl implements CalculationHelper{
	
	private static final Logger log = LoggerFactory.getLogger(CalculationHelperImpl.class);

	//values used to rounding
	public static final Integer NON_QUANTIFIBLE_NUMBER_ACCURACY = 64;
	public static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
	
	@Override
	public BigDecimal addNumbers(List<BigDecimal> numbers) {
		long start = System.currentTimeMillis();
		BigDecimal result = numbers.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
		log.debug("addNumbers() in: " + (System.currentTimeMillis() - start) + " [ms]");
		return result;
	}

	@Override
	public BigDecimal subtractNumbers(List<BigDecimal> numbers) {
		long start = System.currentTimeMillis();
		BigDecimal result = numbers.get(0).subtract(addNumbers(numbers.subList(1, numbers.size())));
		log.debug("subtractNumbers() in: " + (System.currentTimeMillis() - start) + " [ms]");
		return result;
	}

	@Override
	public BigDecimal divideNumbers(List<BigDecimal> numbers) {
		long start = System.currentTimeMillis();
		BigDecimal result = numbers.get(0).divide(multiplyNumbers(numbers.subList(1, numbers.size())), NON_QUANTIFIBLE_NUMBER_ACCURACY, ROUNDING_MODE);
		log.debug("divideNumbers() in: " + (System.currentTimeMillis() - start) + " [ms]");
		return result;
	}

	@Override
	public BigDecimal multiplyNumbers(List<BigDecimal> numbers) {
		long start = System.currentTimeMillis();
		BigDecimal result = numbers.stream().reduce(BigDecimal.ONE, BigDecimal::multiply);
		log.debug("multiplyNumbers() in: " + (System.currentTimeMillis() - start) + " [ms]");
		return result;
	}

	
}
