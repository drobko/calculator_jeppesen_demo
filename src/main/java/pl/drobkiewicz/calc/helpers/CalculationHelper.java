package pl.drobkiewicz.calc.helpers;

import java.math.BigDecimal;
import java.util.List;

public interface CalculationHelper {
	
	/**
	 * Method to add all numbers sent in list as parameter
	 * @param numbers
	 * @return
	 */
	public BigDecimal addNumbers(List<BigDecimal> numbers);
	
	/**
	 * Method to subtract numbers sent in list as parameter.
	 * The subtraction is performed from the first number
	 * @param numbers
	 * @return
	 */
	public BigDecimal subtractNumbers(List<BigDecimal> numbers);
	
	/**
	 * Method to subtract numbers sent in list as parameter.
	 * The dividing is performed from the first number
	 * @param numbers
	 * @return
	 */
	public BigDecimal divideNumbers(List<BigDecimal> numbers);
	
	/**
	 * Method to multiply numbers sent in list as parameter.
	 * @param numbers
	 * @return
	 */
	public BigDecimal multiplyNumbers(List<BigDecimal> numbers);
	
	

}
