package pl.drobkiewicz.calc.enums;

import java.util.Arrays;
import java.util.Optional;

/**
 * Enum created to represent operation
 * @author filip drobkiewicz
 */
public enum MathematicalOperationEnum {

	ADDITION,
	DIVISION,
	MULTIPLICATION,
	SUBTRACTION;
	
	public static Optional<MathematicalOperationEnum> getByName(String name) {
		return Arrays.stream(MathematicalOperationEnum.values()).filter(operationEnum -> name.equals(operationEnum.name())).findAny();
	}
}
