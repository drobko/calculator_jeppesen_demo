package pl.drobkiewicz.calc.enums;

public enum InvalidParameterExceptionTypes {

	WRONG_NUMBERS_OF_PARAMS("Invalid number of parameters!"),
	INVALID_PARAMETERS_TYPE("Invalid parameters type!"),
	CANNOT_DIVIDE_BY_ZERO("Cannot divide by zero!");
	
	private String message;
	
	private InvalidParameterExceptionTypes(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return this.message;
	}
	
}
